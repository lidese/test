﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lec_3_example
{
    public partial class GoodEdit : Form
    {
        public GoodEdit()
        {
            InitializeComponent();

            this.tb_good_name.Text = GoodData.name;
            this.tb_good_quantity.Text 
                = GoodData.quantity.ToString();
            this.tb_good_price.Text
                = GoodData.price.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int price = GetPrice();

            if (price != -1)
            {
                GoodData.price = price;
                this.Close();
            }
        }


        private int GetPrice()
        {
            int price = -1;
            try
            {
                price = System.Convert.ToInt32
                                        (tb_good_price.Text);

                if (price < 1)
                {
                    throw new Exception();
                }

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, 
                    "Ошибка при вводе значения цены!");
            }

            return price;
        }




    }
}
