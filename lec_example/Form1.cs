﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using System.Data.SqlClient;


namespace lec_3_example
{


    public partial class Form1 : Form
    {
        ShopWorker shop;
        GoodEdit GoodEditForm;

        public Form1()
        {
            InitializeComponent();
            shop = new ShopWorker();

            dataGridView1.ReadOnly = true;
            dataGridView1.SelectionMode =
                DataGridViewSelectionMode.FullRowSelect;

        }


        private void button1_Click(object sender, EventArgs e)
        {
            shop.ConnectShop();

            dataGridView1.DataSource = shop.ShowGoods(); ;
            dataGridView1.Refresh();
        }

        private void bt_good_edit_Click(object sender, EventArgs e)
        {
            int id = dataGridView1.CurrentRow.Index;
            GoodData.id =
                System.Convert.ToInt32(
                    dataGridView1.Rows[id].Cells[0].Value);

            GoodData.name =
                System.Convert.ToString(
                    dataGridView1.Rows[id].Cells[1].Value);

            GoodData.quantity =
                System.Convert.ToInt32(
                    dataGridView1.Rows[id].Cells[2].Value);

            GoodData.price =
                System.Convert.ToInt32(
                    dataGridView1.Rows[id].Cells[3].Value);
            
            GoodEditForm = new GoodEdit();
            GoodEditForm.ShowDialog();

            shop.UpdateGood();

            dataGridView1.DataSource = shop.ShowGoods(); ;
            dataGridView1.Refresh();
            
            


        }


    }


    public static class GoodData
    {
        public static int id = 0;
        public static string name = "";
        public static int quantity = 0;
        public static int price = 0;
    }


    public class ShopWorker
    {
        SqlConnection Connection;

        DataTable Table;
        SqlDataAdapter Adapter;

        string str_conn = "Data Source=1-311-DEN;Initial Catalog=shop;Integrated Security=True";

        public ShopWorker()
        {
            Connection = new SqlConnection();
            Adapter = new SqlDataAdapter();
            Table = new DataTable();
        }

        public void ConnectShop()
        {
            Connection.ConnectionString = str_conn;
            Connection.Open();
        }


        public DataTable ShowGoods()
        {
            DataTable table = new DataTable();

            Adapter.SelectCommand =
                new SqlCommand("select * from Товар", Connection);

            Adapter.Fill(table);

            return table;
        }

        public void UpdateGood()
        {
            SqlCommand command = new SqlCommand();
            command.Connection = Connection;
            command.CommandText =
                "update Товар set Цена=" + GoodData.price +
                " where id=" + GoodData.id;
            
            command.ExecuteNonQuery();

        }




    }



}
