﻿namespace lec_3_example
{
    partial class GoodEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_good_name = new System.Windows.Forms.TextBox();
            this.tb_good_price = new System.Windows.Forms.TextBox();
            this.tb_good_quantity = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(75, 80);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Наименование";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(75, 126);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Количество";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(75, 174);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Цена";
            // 
            // tb_good_name
            // 
            this.tb_good_name.Location = new System.Drawing.Point(217, 77);
            this.tb_good_name.Name = "tb_good_name";
            this.tb_good_name.Size = new System.Drawing.Size(229, 26);
            this.tb_good_name.TabIndex = 3;
            // 
            // tb_good_price
            // 
            this.tb_good_price.Location = new System.Drawing.Point(217, 171);
            this.tb_good_price.Name = "tb_good_price";
            this.tb_good_price.Size = new System.Drawing.Size(229, 26);
            this.tb_good_price.TabIndex = 4;
            // 
            // tb_good_quantity
            // 
            this.tb_good_quantity.Location = new System.Drawing.Point(217, 123);
            this.tb_good_quantity.Name = "tb_good_quantity";
            this.tb_good_quantity.Size = new System.Drawing.Size(229, 26);
            this.tb_good_quantity.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(285, 275);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 31);
            this.button1.TabIndex = 6;
            this.button1.Text = "Ok";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // GoodEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 342);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tb_good_quantity);
            this.Controls.Add(this.tb_good_price);
            this.Controls.Add(this.tb_good_name);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "GoodEdit";
            this.Text = "GoodEdit";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_good_name;
        private System.Windows.Forms.TextBox tb_good_price;
        private System.Windows.Forms.TextBox tb_good_quantity;
        private System.Windows.Forms.Button button1;
    }
}